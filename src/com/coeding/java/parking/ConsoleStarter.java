package com.coeding.java.parking;

import java.util.Scanner;

/**
 * 
 * Bike parking station
 * 1. keyboard input ( in, out ... )
 * 2. parking slot : fixed numbers of max bike
 * 3. price : outTime - inTime
 * @author Administrator
 *
 */
public class ConsoleStarter {

	public static void main(String[] args) {
		Scanner sc = new Scanner( System.in );
		String[] space = new String[10];
		// initialized null
		// "carNumber,inTime" <-- CSV formatted
		// String.split(",")
		
		do{
			System.out.println("cmd >");
			String cmd = sc.nextLine();// return String
			if( cmd.equals("quit") ) {
				break;// stop loop
			}
			if( cmd.equals("in") ) {// locally defined
				int i;
				// search
				for(i=0; i<space.length; i++) {
					if( space[i] == null ) {// then empty
						break;
					}
				}// end loop
				
				// after search
				if(i == space.length) {//
					System.out.println("full ");
				}else { // i < space.length
					System.out.println("has empty");
					// can do parking
					System.out.println("car number > ");
					String carNumber = sc.nextLine();
					System.out.println(carNumber+" in -->" + i);
					space[i] = 	carNumber+","
								+System.currentTimeMillis();
				}
			}
			if( cmd.equals("out") ) {
				System.out.println("car number : ");
				String carNumber = sc.nextLine();
				int i;
				// searching
				for(i=0; i < space.length; i++) {
					if( space[i] != null ) {
						String[] car = space[i].split(",");
						// car[0]:carNumber car[1]
						if( carNumber.equals(car[0])) {
							System.out.println(carNumber+" out <--"+i);
							long outTime = System.currentTimeMillis();
							long inTime = Long.parseLong(car[1]);
							long price = (outTime-inTime)*1000;
							System.out.println("price : "+ price);
							space[i] = null; //<-- is empty
							break;// or continue
						}
					}
				}
			}
		}while(true);
		
		sc.close();
		System.out.println("bye ... ");
	}// end main ( program out )

	public static void main01(String[] args) {
		Scanner sc = new Scanner( System.in );
		String[] space = new String[10];
		long[] timetable = new long[10];
		
		do{
			System.out.println("cmd >");
			String cmd = sc.nextLine();// return String
			if( cmd.equals("quit") ) {
				break;// stop loop
			}
			if( cmd.equals("in") ) {// locally defined
				int i;
				// search
				for(i=0; i<space.length; i++) {
					if( space[i] == null ) {// then empty
						break;
					}
				}// end loop
				
				// after search
				if(i == space.length) {//
					System.out.println("full ");
				}else { // i < space.length
					System.out.println("has empty");
					// can do parking
					System.out.println("car number > ");
					String carNumber = sc.nextLine();
					System.out.println(carNumber+" in -->" + i);
					space[i] = carNumber;//<-- parked in [i]
					timetable[i] = System.currentTimeMillis();
				}
			}
			if( cmd.equals("out") ) {
				System.out.println("car number : ");
				String carNumber = sc.nextLine();
				int i;
				// searching
				for(i=0; i < space.length; i++) {
					if( carNumber.equals(space[i])) {
						// to do when searched car
						System.out.println(carNumber+" out <--"+i);
						long outTime = System.currentTimeMillis();
						long inTime = timetable[i];
						long price = (outTime-inTime)*1000;
						System.out.println("price : "+ price);
						space[i] = null; //<-- is empty
						break;// or continue
					}
				}
			}
		}while(true);
		
		sc.close();
		System.out.println("bye ... ");
	}// end main ( program out )

}







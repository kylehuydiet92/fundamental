package com.coeding.java.oop;

import java.util.Scanner;

/**
 * 
 * blog concepts
 * - have articles
 * - writer
 * blog CRUD
 * - console : keyboard and monitor
 * 
 * @author Administrator
 *
 */
public class Starter {
	static Scanner sc;
	static Article[] list;
	static int seq;
	public static void main(String[] args) {
		// TODO: program entry point ( start )
		sc = new Scanner( System.in );
		list = new Article[10];
		seq = 0;
		while( true ) {
			System.out.println(">");
			String cmd = sc.nextLine();
			if( cmd.equals("new")) {// Create
				registArticle();
			}
			if( cmd.equals("edit")) {// Update
				System.out.println("select article's index > ");
				cmd = sc.nextLine();// as String "1"
				int idx = Integer.parseInt(cmd);//1
				if( idx < seq ) {// if seq 3, [0][1][2]
					editArticle(idx); // go to edit [idx]article
				}else {
					System.out.println("invaild index");
				}
			}
			if( cmd.equals("search")) {
				System.out.println("search writer > ");
				cmd = sc.nextLine();
				Article[] rs = searchArticleByWriter(cmd);
				// writer can write many articles
			}
			
			
		}// end while
	}

	private static Article[] searchArticleByWriter(String cmd) {
		// TODO Auto-generated method stub
		return null;
	}

	private static void editArticle(int idx) {
		Article article = list[idx];// get reference 
		System.out.println("Title > "+ article.getTitle());
		String title = sc.nextLine();
		// press only <enter> , empty string
		if( title.length() > 0) {			// have input data
			article.setTitle(title);// over write data
		}
		System.out.println("Writer > "+ article.getWriterName());
		String name = sc.nextLine();
		if( !name.isEmpty()) {			// have input data
			article.setWriterName(name);// over write data
		}
		System.out.println("Content > "+ article.getContent());
		String content = sc.nextLine();
		if( content.length() > 0) {			// have input data
			article.setContent(content);
		}
		
		// quiz ? tai sao khong co code below
		//	list[idx] = article;	already set via setter
		
	}

	private static void registArticle() {
		// data read from keyboard
		System.out.println("Title > ");
		String title = sc.nextLine();
		System.out.println("Writer > ");
		String name = sc.nextLine();
		System.out.println("Content > ");
		String content = sc.nextLine();
		// save data in Article instance
		Article article = new Article();
		article.setTitle(title);
		article.setWriterName(name);
		article.setContent(content);			
		System.out.println(article);
		if( seq < list.length  ) {
			list[ seq ] = article;// need index of array
			++seq;
			// ArrayIndexOutOfBounds
		}
	}

}

package com.coeding.java.oop;

public class Car {
	private static Car instance = null;
	private Car() {}
	public static Car getInstance() {
		// for JDBC, Spring
		if( instance == null ) {
			instance = new Car();
		}
		return instance;
		// instace has one in memory
		// Singleton pattern
	}

	public void forwarding() {
		System.out.println("go go ");
	}
	public void backwarding() {
		System.out.println("back back ");		
	}

}
